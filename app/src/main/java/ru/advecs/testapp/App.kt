package ru.advecs.testapp

import android.app.Application
import android.os.StrictMode
import android.os.StrictMode.VmPolicy

class App : Application()
{
	override fun onCreate()
	{
		super.onCreate()
		appInstance = this

		if(BuildConfig.DEBUG)
		{
			StrictMode.setThreadPolicy(
				StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites()
					.detectNetwork() // or .detectAll() for all detectable problems
					.penaltyLog().build()
									  )
			StrictMode.setVmPolicy(
				VmPolicy.Builder().detectLeakedSqlLiteObjects() //.detectLeakedClosableObjects()
					.penaltyLog().penaltyDeath().build()
								  )
		}
	}

	companion object
	{
		@JvmField
        var appInstance: App? = null
	}
}