package ru.advecs.testapp.base

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import ru.advecs.testapp.App
import ru.advecs.testapp.base.mvp.MvpView
import ru.advecs.testapp.utils.ViewUtils

open class BaseActivity() : AppCompatActivity(), MvpView
{
	private var requestServerDialog: AlertDialog? = null
	override fun onCreate(savedInstanceState: Bundle?)
	{
		super.onCreate(savedInstanceState)
		initView()
	}

	private fun initView()
	{
		requestServerDialog = ViewUtils.setProgressDialog(this)
	}

	override fun showMessage(messageResId: Int)
	{
		runOnUiThread { Toast.makeText(App.appInstance, messageResId, Toast.LENGTH_SHORT).show() }
	}

	override fun showMessage(message: String?)
	{
		runOnUiThread(Runnable { Toast.makeText(App.appInstance, message, Toast.LENGTH_SHORT).show() })
	}

	override fun startProgress()
	{
		runOnUiThread(object : Runnable
		              {
			              override fun run()
			              {
				              if (!requestServerDialog!!.isShowing)
				              {
					              requestServerDialog!!.show()
				              }
			              }
		              })
	}

	override fun stopProgress()
	{
		runOnUiThread(object : Runnable
		              {
			              override fun run()
			              {
				              if (requestServerDialog!!.isShowing)
				              {
					              requestServerDialog!!.dismiss()
				              }
			              }
		              })
	}

/*	fun logout()
	{
		startActivity(Intent(this, LoginActivity::class.java))
	}*/
}