package ru.advecs.testapp.base.entities

import ru.advecs.testapp.storage.remote.entities.FileInfo

abstract class Estate
{
    abstract val id: String?
    abstract val creationDate: String?
    abstract val lastUpdateDate: String?
    abstract val dealType: String?
    abstract val region: String?
    abstract val locality: String?
    abstract val address: String?
    abstract val price: String?
    abstract val price1m: String?
    abstract val quality: String?
    abstract val allSpace: String?
    abstract val phone: String?
    abstract val description: String?
    abstract val latitude: String?
    abstract val longitude: String?
    abstract val agent: String?
    abstract val agentId: String?
    abstract val agentPhone: String?
    abstract val agentName: String?
    abstract val agentSurname: String?
    abstract val agentPhoto: String?
    abstract val agency: String?
    abstract val agencyPhone: String?
    abstract val agencyLogo: String?
    abstract val agreementBeginDate: String?
    abstract val agreementEndDate: String?
    abstract val agreementType: String?
    abstract val typeDbId: String?
    abstract val baseId: String?
    abstract val images: List<FileInfo?>?
}