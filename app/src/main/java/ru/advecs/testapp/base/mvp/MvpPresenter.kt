package ru.advecs.testapp.base.mvp

interface MvpPresenter<V : MvpView?>
{
	fun attachView(mvpView: V)
	fun viewIsReady()
	fun detachView()
	fun cleanup()
}