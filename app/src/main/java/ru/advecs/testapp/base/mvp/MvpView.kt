package ru.advecs.testapp.base.mvp

interface MvpView
{
	fun showMessage(messageResId: Int)
	fun showMessage(message: String?)
	fun startProgress()
	fun stopProgress()
}