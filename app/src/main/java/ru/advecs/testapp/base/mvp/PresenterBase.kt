package ru.advecs.testapp.base.mvp

abstract class PresenterBase<T : MvpView?> : MvpPresenter<T>
{
	var view: T? = null
		private set

	override fun attachView(mvpView: T)
	{
		view = mvpView
	}

	override fun detachView()
	{
		view = null
	}

	protected val isViewAttached: Boolean
		get() = view != null

	override fun cleanup()
	{
	}
}