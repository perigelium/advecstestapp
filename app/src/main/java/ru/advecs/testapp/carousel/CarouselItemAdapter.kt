package ru.advecs.testapp.carousel

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import ru.advecs.testapp.R
import ru.advecs.testapp.databinding.CarouselItemBinding

class CarouselItemAdapter(context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
	private val cardWidth: Int
	private var cardHeight: Int
	private var listItemViewData: MutableList<ItemViewData?> = ArrayList()
	private var mListner: IItemViewDataListListener? = null
	
	fun setItems(mListItemViewData: MutableList<ItemViewData?>)
	{
		listItemViewData = mListItemViewData;
		notifyDataSetChanged()
	}

	fun setListner(listner: IItemViewDataListListener?)
	{
		mListner = listner
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder
	{
		val binding = DataBindingUtil.inflate<CarouselItemBinding>(LayoutInflater.from(parent.context),
																   R.layout.carousel_item, parent, false)
		binding.root.layoutParams.width = cardWidth
		binding.root.layoutParams.height = cardHeight
		return object : RecyclerView.ViewHolder(binding.root)
		{}
	}

	override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int)
	{
		DataBindingUtil.getBinding<ViewDataBinding>(holder.itemView)!!
				.setVariable(BR.data, listItemViewData[position])
	}

	override fun getItemCount(): Int
	{
		return listItemViewData.size
	}

	interface IItemViewDataListListener
	{
		fun onItemDeleted(position: Int)
	}

	init
	{
		cardWidth = ViewGroup.LayoutParams.MATCH_PARENT//computeCardWidth(context)
		cardHeight = ViewGroup.LayoutParams.MATCH_PARENT //computeCardHeight(context, cardWidth);
	}
}