package ru.advecs.testapp.carousel

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import ru.advecs.testapp.App.Companion.appInstance


object ImageUtil
{
	//val dpi = appInstance!!.getResources().getDisplayMetrics().density

	@JvmStatic
	@BindingAdapter("imageUrl")
	fun loadImage(view: ImageView?, url: String?)
	{
		if (url != null && view!=null)
		{
			Glide.with(appInstance!!).load(url).centerInside().into(view)
			//.placeholder(R.drawable.map_default)
					//.error(R.drawable.map_default)
					//.into(view!!)

/*			val carousel_height: Int = appInstance!!.getResources().getInteger(R.integer.carousel_height)
			val heightInPixels: Int = (carousel_height*dpi).toInt()
			Picasso.get().cancelRequest(view)
			Picasso.get().load(url).resize(0, heightInPixels).centerInside().into(view)*/
		}
	}
}