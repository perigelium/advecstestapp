package ru.advecs.testapp.carousel

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import ru.advecs.testapp.R

class ItemDecoration : RecyclerView.ItemDecoration()
{
	override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State)
	{
		val spacingPx = view.resources.getDimensionPixelSize(R.dimen.item_spacing)
		if (parent.adapter != null)
		{
			outRect.left = spacingPx
			if (parent.getChildAdapterPosition(
							view) == parent.adapter!!.itemCount - 1 && parent.layoutDirection == View.LAYOUT_DIRECTION_LTR || parent.getChildAdapterPosition(
							view) == 0 && parent.layoutDirection == View.LAYOUT_DIRECTION_RTL)
			{
				outRect.right = spacingPx
			}
		}
	}
}