package ru.advecs.testapp.carousel;

import android.net.Uri;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import ru.advecs.testapp.utils.FileUtils;

import static ru.advecs.testapp.App.appInstance;

public class ItemViewData
{
	public String id;
	public String name;
	public String localPath;

	public String commentary;
	public final String imageUrl;
	public String text;
	public String subtext;
	public String socialFooter;

	public ItemViewData(String imageUrl)
	{
		this.imageUrl = imageUrl;

		id = UUID.randomUUID().toString();

		try
		{
			final File file = FileUtils.copyFileFromUri(
					appInstance, Uri.parse(imageUrl),
			                                            FileUtils.getImagesDir().getAbsolutePath());

			if (file != null && file.exists())
			{
				localPath = file.getAbsolutePath();
				name = file.getName();
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
