package ru.advecs.testapp.main

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import ru.advecs.testapp.R
import ru.advecs.testapp.base.entities.Estate
import ru.advecs.testapp.carousel.CarouselItemAdapter
import ru.advecs.testapp.carousel.ItemDecoration
import ru.advecs.testapp.carousel.ItemViewData
import ru.advecs.testapp.storage.remote.entities.FileInfo
import ru.advecs.testapp.storage.remote.entities.Flat
import java.util.*


class AgentEstateAdapter(private val resource: Int, private val ctx: Context) :
    RecyclerView.Adapter<AgentEstateAdapter.ViewHolder>()
{
    private val mList: MutableList<Estate>? = ArrayList()
    private var mListner: ITListListener? = null

    fun setList(list: List<Estate>?)
    {
        mList!!.clear()
        mList.addAll(list!!)
        notifyDataSetChanged()
    }

    interface ITListListener
    {
        //fun OnOpenEstateDetails(estate: Estate?)
    }

    fun setListner(listner: ITListListener?)
    {
        mListner = listner
    }

    override fun getItemId(position: Int): Long
    {
        return if (mList == null)
        {
            0
        } else position.toLong()
    }

    override fun getItemCount(): Int
    {
        return mList?.size ?: 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        val rootView = LayoutInflater.from(parent.context).inflate(resource, parent, false)
        return ViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        val estate: Estate = mList!![position] ?: return

        val strAddress = "Адрес: " + estate.address
        holder.tvEstateAddress.text = strAddress
        val strParams = estate.price + " 000\u20BD \u2022 " + estate.allSpace + "м\u00B2"
        holder.tvEstateParams.text = strParams
        var strAdvType = ""

        if (estate is Flat)
        {
            val flat = estate

            if (flat.getStation() != null)
            {
                val strRoomCount = flat.getRooms()
                strAdvType = "$strRoomCount-комн. квартира"
                holder.tvEstateType.text = strAdvType
                val strMetro = "Метро: " + flat.getStation()
                holder.tvNearestMetroAddress.text = strMetro
            }
        }

        val itemAdapter = CarouselItemAdapter(ctx)
        holder.rvCarouselView.adapter = itemAdapter

        //StartSnapHelper().attachToRecyclerView(holder.rvCarouselView)

        holder.rvCarouselView.addItemDecoration(ItemDecoration())

        val images: List<FileInfo?>? = estate.images

        val linearLayoutManager = LinearLayoutManager(ctx, RecyclerView.HORIZONTAL, false)
        //holder.rvCarouselView.layoutManager = linearLayoutManager as LinearLayoutManager

/*        holder.rvCarouselView.onFlingListener = object : RecyclerView.OnFlingListener()
        {
            override fun onFling(velocityX: Int, velocityY: Int): Boolean
            {
                Handler().postDelayed({
                                          switchMorePhotosPanel(images.size, holder, linearLayoutManager)
                                      }, 2000)
                return false
            }
        }*/

        holder.rvCarouselView.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback()
                                                           {
                                                               override fun onPageSelected(position: Int)
                                                               {
                                                                   Handler().postDelayed({
                                                                                             switchMorePhotosPanel(
                                                                                                 images!!.size,
                                                                                                 holder,
                                                                                                                  )
                                                                                         }, 1000)
                                                                   super.onPageSelected(position)
                                                               }
                                                           })

        holder.flMorePhotosPanel.setOnClickListener {
            holder.rvCarouselView.post {
                //holder.rvCarouselView.smoothScrollToPosition(itemAdapter.itemCount - 1)
                removeMoreItemsPanel(holder)
            }
        }

        removeMoreItemsPanel(holder)


        val listItemViewData: MutableList<ItemViewData?> = ArrayList()

        for (fileInfo in images!!)
        {
            listItemViewData.add(ItemViewData(fileInfo?.pathBig))
        }
        addItemsViewData(listItemViewData, itemAdapter)
        switchMorePhotosPanel(images.size, holder)

        holder.tvShare.setOnClickListener(View.OnClickListener { shareItemDataToMessengers(advertId = estate.id, typeDbId = estate.typeDbId, advType = strParams + " " + strAdvType) })
    }

    private fun addItemsViewData(
        listItemViewData: MutableList<ItemViewData?>?,
        carouselItemAdapter: CarouselItemAdapter,
                        )
    {
        val itemsViewData: MutableList<ItemViewData?> = ArrayList()

        itemsViewData.let(fun(list1: MutableList<ItemViewData?>): Boolean?
                          {
                              return listItemViewData?.let(list1::addAll)
                          })

        carouselItemAdapter.setItems(itemsViewData)
    }

    private fun switchMorePhotosPanel(listSize: Int, holder: ViewHolder)
    {
        if (listSize > 2)
        {
            val lastVisibleItemPosition: Int = holder.rvCarouselView.currentItem//linearLayoutManager.findFirstCompletelyVisibleItemPosition()
            //if (lastVisibleItemPosition < 1) lastVisibleItemPosition = 1
            if (lastVisibleItemPosition < listSize - 1)
            {
                holder.flMorePhotosPanel.visibility = View.VISIBLE

                val strMoreItemsQuant: String = (listSize - lastVisibleItemPosition - 1).toString() + "+"
                holder.tvMorePhotosQuant.text = strMoreItemsQuant
            } else
            {
                removeMoreItemsPanel(holder)
            }
        } else
        {
            removeMoreItemsPanel(holder)
        }
    }

    fun removeMoreItemsPanel(holder: ViewHolder)
    {
        holder.flMorePhotosPanel.visibility = View.GONE
    }

    fun shareItemDataToMessengers(advertId:String?, typeDbId:String?, advType:String?)
    {
        val link:String = advType + " https://emls.ru/fullinfo/" + typeDbId + "/" + advertId + ".html"

        val intent:Intent = Intent(Intent.ACTION_SEND)
        intent.setType("text/plain")
        intent.putExtra(Intent.EXTRA_SUBJECT, advType)
        intent.putExtra(Intent.EXTRA_TEXT, link)

        val createChooser = Intent.createChooser(intent, "Выберите приложение или абонента")
        ctx.startActivity(createChooser)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val tvEstateType: TextView
        val tvEstateAddress: TextView
        val tvEstateParams: TextView
        val tvNearestMetroAddress: TextView
        val rvCarouselView: ViewPager2
        val flMorePhotosPanel: FrameLayout
        val tvMorePhotosQuant: TextView
        val tvShare: TextView

        init
        {
            tvEstateType = view.findViewById(R.id.tvEstateType)
            tvEstateAddress = view.findViewById(R.id.tvEstateAddress)
            tvEstateParams = view.findViewById(R.id.tvEstateParams)
            tvNearestMetroAddress = view.findViewById(R.id.tvNearestMetroAddress)
            rvCarouselView = view.findViewById(R.id.rvCarouselView)
            flMorePhotosPanel = view.findViewById(R.id.flMorePhotosPanel)
            tvMorePhotosQuant = view.findViewById(R.id.tvMorePhotosQuant)
            tvShare = view.findViewById(R.id.tvShare)
        }
    }
}