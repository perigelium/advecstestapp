package ru.advecs.testapp.main

import android.os.Bundle
import kotlinx.android.synthetic.main.main_activity.*
import ru.advecs.testapp.R
import ru.advecs.testapp.base.BaseActivity
import ru.advecs.testapp.storage.remote.entities.Flat

class MainActivity : BaseActivity(), MainContract.View,
					 AgentEstateAdapter.ITListListener
{
	lateinit var agentEstateAdapter: AgentEstateAdapter
	lateinit var mPresenter: MainContract.Presenter
	override fun onCreate(savedInstanceState: Bundle?)
	{
		super.onCreate(savedInstanceState)
		setContentView(R.layout.main_activity)
		mPresenter = MainPresenter(this)
		initView()

		mPresenter.attachView(this)

		mPresenter.viewIsReady()
	}

	private fun initView()
	{
		agentEstateAdapter = AgentEstateAdapter(R.layout.estate_object_item, this)
		agentEstateAdapter.setListner(this)
		rvAgentEstateItems.setAdapter(agentEstateAdapter)
	}

	override fun onDestroy()
	{
		super.onDestroy()
		mPresenter.detachView()
		if (isFinishing)
		{
			mPresenter.cleanup()
		}
	}

	override fun close()
	{
		finish()
	}

	override fun updateEstateList(flats: List<Flat>?)
	{
		runOnUiThread { agentEstateAdapter.setList(flats) }
	}
}