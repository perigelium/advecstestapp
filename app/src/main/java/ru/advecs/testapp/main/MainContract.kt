package ru.advecs.testapp.main

import ru.advecs.testapp.base.mvp.MvpPresenter
import ru.advecs.testapp.base.mvp.MvpView
import ru.advecs.testapp.storage.remote.entities.Flat

interface MainContract
{
	interface View : MvpView
	{
		// close screen
		fun close()
        fun updateEstateList(flats: List<Flat>?)
    }

	interface Presenter : MvpPresenter<View?>
}