package ru.advecs.testapp.main

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import ru.advecs.testapp.base.mvp.PresenterBase
import ru.advecs.testapp.storage.remote.FlatsRepo
import ru.advecs.testapp.storage.remote.FlatsUseCase
import ru.advecs.testapp.storage.remote.entities.Flat


class MainPresenter(private val lifecycleOwner: LifecycleOwner) :
    PresenterBase<MainContract.View?>(), MainContract.Presenter
{
    override fun viewIsReady()
    {
        if (isViewAttached) view!!.startProgress()
        subscribeToFlatsRepoData()
        getFlats()
    }

    private fun subscribeToFlatsRepoData()
    {
        val flatsRepo = FlatsRepo.instance!!

        flatsRepo.value = null
        flatsRepo.observe(lifecycleOwner, Observer { pairFlatsResponse ->

            if (pairFlatsResponse == null) return@Observer
            if (isViewAttached) view!!.stopProgress()

            flatsRepo.removeObservers(lifecycleOwner)

            val flats: MutableList<Flat>? = pairFlatsResponse.first

            if (flats == null)
            {
                val errMsg = pairFlatsResponse.second
                if (errMsg != null)
                {
                    view!!.showMessage(errMsg)
                }
            } else
            {
                view?.updateEstateList(flats)
            }
        })

    }

    fun getFlats()
    {
        FlatsUseCase.findFlats("101")
    }
}