package ru.advecs.testapp.storage

object Constants
{
    const val DATA_JSON_OBJECT = "data_json_object"
    const val ACTION_NAME = "action_name"
    const val ACTION_GET_FLATS = "action_get_flats"
}