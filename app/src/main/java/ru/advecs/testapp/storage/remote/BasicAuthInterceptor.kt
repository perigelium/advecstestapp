package ru.advecs.testapp.storage.remote

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class BasicAuthInterceptor : Interceptor
{
	@Throws(IOException::class)
	override fun intercept(chain: Interceptor.Chain): Response
	{
		val credentials = "ff5194e82dfc8a6dee8c94c6c5bb5523"//Credentials.basic(login, password)
		val request = chain.request()
		val headers = request.headers().newBuilder()
			.add("Authorization", credentials)
			.build()
		val authenticatedRequest = request.newBuilder().headers(headers).build()
		return chain.proceed(authenticatedRequest)
	}
}