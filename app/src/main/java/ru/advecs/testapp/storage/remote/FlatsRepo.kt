package ru.advecs.testapp.storage.remote

import android.util.ArrayMap
import androidx.core.util.Pair
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import ru.advecs.testapp.storage.remote.entities.FileInfo
import ru.advecs.testapp.storage.remote.entities.Flat
import ru.advecs.testapp.utils.ErrorUtils.getRetrofitErrorMessage
import java.io.IOException
import java.util.*

class FlatsRepo : MutableLiveData<Pair<MutableList<Flat>?, String?>>()
{
    private val retrofitService: RetrofitAPI
    fun getFlats(agentId: String): Pair<List<Flat>?, String>
    {
        var errMsg: String? = null
        val flats: MutableList<Flat> = ArrayList()
        val mapAgent = ArrayMap<String, String>()
        mapAgent["agent[]"] = agentId
        try
        {
            val call: Call<ResponseBody>? = retrofitService.getFlats(mapAgent)
            if (call == null)
            {
                errMsg = "call getFlats is null"
                return Pair(null, errMsg)
            }
            val response = call.execute()
            if (response.isSuccessful)
            {
                if (response.body() != null)
                {
                    var strResponseBody: String? = null
                    strResponseBody = response.body()!!.string().toString()
                    parseResponseString(flats, strResponseBody)

                    postValue(Pair(flats, errMsg))

                    response.body()!!.close()
                }
            } else
            {
                errMsg = getRetrofitErrorMessage(response)
            }
        } catch (e: IOException)
        {
            e.printStackTrace()
            errMsg = e.localizedMessage
        }
        return Pair(flats, errMsg)
    }

    private fun parseResponseString(flats: MutableList<Flat>, strResponseBody: String)
    {
        val gson = Gson()
        val map = gson.fromJson(strResponseBody, JsonObject::class.java)
        for (i in 0 until Int.MAX_VALUE)
        {
            val jsonElement = map[i.toString()] ?: break
            val jsonObjectX = jsonElement.asJsonObject
            val jsonElementFlat = jsonObjectX["object"] ?: break
            val flat = gson.fromJson(jsonElementFlat.toString(), Flat::class.java) ?: break
            val fileInfos: MutableList<FileInfo> = ArrayList()
            for (j in 0 until Int.MAX_VALUE)
            {
                val jsonObjectFlat = jsonElementFlat.asJsonObject
                val jsonElementX = jsonObjectFlat[j.toString()] ?: break
                val jsonObjectImage = jsonElementX.asJsonObject ?: break
                val jsonElementImage = jsonObjectImage["image"] ?: break
                val fileInfo = gson.fromJson(
                    jsonElementImage.toString(), FileInfo::class.java
                                            )
                if (fileInfo != null)
                {
                    fileInfos.add(fileInfo)
                }
            }
            flat.images = fileInfos
            flats.add(flat)
        }
    }

    companion object
    {
        @JvmStatic
        @Volatile
        var instance: FlatsRepo? = null
            get()
            {
                if (field == null)
                {
                    field = FlatsRepo()
                }
                return field
            }
            private set
    }

    init
    {
        retrofitService = RetrofitAPI.retrofit.create(RetrofitAPI::class.java)
    }
}