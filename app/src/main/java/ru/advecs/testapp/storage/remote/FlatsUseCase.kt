package ru.advecs.testapp.storage.remote

import java.util.concurrent.Executors

object FlatsUseCase
{
    @JvmStatic
    fun findFlats(tsId: String)
    {
        val runnable = Runnable { FlatsRepo.instance!!.getFlats(tsId) }
        Executors.newSingleThreadExecutor().execute(runnable)
    }
}