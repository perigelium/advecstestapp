package ru.advecs.testapp.storage.remote

import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.QueryMap
import ru.advecs.testapp.BuildConfig

interface RetrofitAPI
{

	@Headers("Accept: application/json")
	@GET("flats")
	fun getFlats(@QueryMap params:Map<String, String>) : Call<ResponseBody>?

	companion object
	{
		fun createOkHttpClient(): OkHttpClient?
		{
				val httpClient = OkHttpClient.Builder()
				/*            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);*/
				val basicAuthInterceptor = BasicAuthInterceptor()
				if (!httpClient.interceptors().contains(basicAuthInterceptor))
				{ //httpClient.addInterceptor(loggingInterceptor)
					httpClient.addInterceptor(basicAuthInterceptor)
					return httpClient.build()
				}
			return null
		}

		@JvmField
        val retrofit = Retrofit.Builder().baseUrl(BuildConfig.API_BASE_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.addConverterFactory(ScalarsConverterFactory.create()).client(createOkHttpClient()).build()

	}
}