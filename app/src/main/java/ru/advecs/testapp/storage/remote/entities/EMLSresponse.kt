package ru.advecs.testapp.storage.remote.entities

import com.google.gson.JsonObject
import java.util.*

class EMLSresponse
{
    var allcount: String? = null
    var pagelimit: String? = null
    var pagenumber: String? = null
    var data: HashMap<Int, JsonObject>? = null
}