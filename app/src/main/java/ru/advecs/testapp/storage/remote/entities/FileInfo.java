package ru.advecs.testapp.storage.remote.entities;

import com.google.gson.annotations.SerializedName;

public class FileInfo
{
	public String path;
	@SerializedName("path-big")
	public String pathBig;
	@SerializedName("path-medium")
	public String pathMedium;
	@SerializedName("path-small")
	public String pathSmall;

	public String comment;
}
