package ru.advecs.testapp.storage.remote.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.advecs.testapp.base.entities.Estate;

public class Flat extends Estate
{
    public String id;
    @SerializedName("creation-date")
    public String creationDate;
    @SerializedName("last-update-date")
    public String lastUpdateDate;
    @SerializedName("deal-type")
    public String dealType;
    public String rooms;
    public String region;
    public String locality;
    public String address;
    public String station;
    @SerializedName("station-distance")
    public String stationDistance;
    @SerializedName("station-direction")
    public String stationDirection;
    public String stationhowget;
    public String price;
    public String price1m;
    @SerializedName("rooms-type")
    public String roomsType;
    @SerializedName("building-type")
    public String buildingType;
    @SerializedName("building-series")
    public String buildingSeries;
    @SerializedName("building-nput")
    public String buildingInput;
    @SerializedName("property-type")
    public String propertyType;
    public String tenants;
    @SerializedName("ceiling-height")
    public String ceilingHeight;
    public String lift;
    public String balcony;
    @SerializedName("window-view")
    public String windowView;
    public String quality;
    public String phone;
    public String bathroom;
    public String bath;
    @SerializedName("hotwater-supply")
    public String hotwaterSupply;
    @SerializedName("rubbish-chute")
    public String rubbishChute;
    @SerializedName("floor-covering")
    public String floorCovering;
    @SerializedName("all-space")
    public String allSpace;
    public String living;
    @SerializedName("kitchen-space")
    public String kitchenSpace;
    public String floor;
    @SerializedName("floors-total")
    public String floorsTotal;
    @SerializedName("groundfloor-height")
    public String groundfloorHeight;
    public String description;
    public String latitude;
    public String longitude;
    public String agent;
    @SerializedName("agent-id")
    public String agentId;
    @SerializedName("agent-phone")
    public String agentPhone;
    @SerializedName("agent-name")
    public String agentName;
    @SerializedName("agent-surname")
    public String agentSurname;
    @SerializedName("agent-photo")
    public String agentPhoto;
    public String agency;
    @SerializedName("agency-phone")
    public String agencyPhone;
    @SerializedName("agency-logo")
    public String agencyLogo;
    @SerializedName("agreement-begin-date")
    public String agreementBeginDate;
    @SerializedName("agreement-end-date")
    public String agreementEndDate;
    @SerializedName("agreement-type")
    public String agreementType;
    @SerializedName("typedb-id")
    public String typeDbId;
    @SerializedName("base-id")
    public String baseId;

    public List<FileInfo> images;

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public String getCreationDate()
    {
        return creationDate;
    }

    @Override
    public String getLastUpdateDate()
    {
        return lastUpdateDate;
    }

    @Override
    public String getDealType()
    {
        return dealType;
    }

    public String getRooms()
    {
        return rooms;
    }

    @Override
    public String getRegion()
    {
        return region;
    }

    @Override
    public String getLocality()
    {
        return locality;
    }

    @Override
    public String getAddress()
    {
        return address;
    }

    public String getStation()
    {
        return station;
    }

    public String getStationDistance()
    {
        return stationDistance;
    }

    public String getStationDirection()
    {
        return stationDirection;
    }

    public String getStationhowget()
    {
        return stationhowget;
    }

    @Override
    public String getPrice()
    {
        return price;
    }

    @Override
    public String getPrice1m()
    {
        return price1m;
    }

    public String getRoomsType()
    {
        return roomsType;
    }

    public String getBuildingType()
    {
        return buildingType;
    }

    public String getBuildingSeries()
    {
        return buildingSeries;
    }

    public String getBuildingInput()
    {
        return buildingInput;
    }

    public String getPropertyType()
    {
        return propertyType;
    }

    public String getTenants()
    {
        return tenants;
    }

    public String getCeilingHeight()
    {
        return ceilingHeight;
    }

    public String getLift()
    {
        return lift;
    }

    public String getBalcony()
    {
        return balcony;
    }

    public String getWindowView()
    {
        return windowView;
    }

    @Override
    public String getQuality()
    {
        return quality;
    }

    @Override
    public String getPhone()
    {
        return phone;
    }

    public String getBathroom()
    {
        return bathroom;
    }

    public String getBath()
    {
        return bath;
    }

    public String getHotwaterSupply()
    {
        return hotwaterSupply;
    }

    public String getRubbishChute()
    {
        return rubbishChute;
    }

    public String getFloorCovering()
    {
        return floorCovering;
    }

    @Override
    public String getAllSpace()
    {
        return allSpace;
    }

    public String getLiving()
    {
        return living;
    }

    public String getKitchenSpace()
    {
        return kitchenSpace;
    }

    public String getFloor()
    {
        return floor;
    }

    public String getFloorsTotal()
    {
        return floorsTotal;
    }

    public String getGroundfloorHeight()
    {
        return groundfloorHeight;
    }

    @Override
    public String getDescription()
    {
        return description;
    }

    @Override
    public String getLatitude()
    {
        return latitude;
    }

    @Override
    public String getLongitude()
    {
        return longitude;
    }

    @Override
    public String getAgent()
    {
        return agent;
    }

    @Override
    public String getAgentId()
    {
        return agentId;
    }

    @Override
    public String getAgentPhone()
    {
        return agentPhone;
    }

    @Override
    public String getAgentName()
    {
        return agentName;
    }

    @Override
    public String getAgentSurname()
    {
        return agentSurname;
    }

    @Override
    public String getAgentPhoto()
    {
        return agentPhoto;
    }

    @Override
    public String getAgency()
    {
        return agency;
    }

    @Override
    public String getAgencyPhone()
    {
        return agencyPhone;
    }

    @Override
    public String getAgencyLogo()
    {
        return agencyLogo;
    }

    @Override
    public String getAgreementBeginDate()
    {
        return agreementBeginDate;
    }

    @Override
    public String getAgreementEndDate()
    {
        return agreementEndDate;
    }

    @Override
    public String getAgreementType()
    {
        return agreementType;
    }

    @Override
    public String getTypeDbId()
    {
        return typeDbId;
    }

    @Override
    public String getBaseId()
    {
        return baseId;
    }

    @Override
    public List<FileInfo> getImages()
    {
        return images;
    }
}
