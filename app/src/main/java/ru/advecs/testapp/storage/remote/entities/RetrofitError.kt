package ru.advecs.testapp.storage.remote.entities

import com.google.gson.annotations.SerializedName

class RetrofitError
{
    @SerializedName("code")
    var code = 0

    @SerializedName("error")
    var errorDetails: String? = null
}