package ru.advecs.testapp.utils

import android.text.Html
import android.util.Log
import okhttp3.MediaType
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Response
import ru.advecs.testapp.BuildConfig
import ru.advecs.testapp.storage.remote.RetrofitAPI
import ru.advecs.testapp.storage.remote.RetrofitAPI.Companion.retrofit
import ru.advecs.testapp.storage.remote.entities.RetrofitError
import java.io.IOException

object ErrorUtils
{
	val className = ErrorUtils::class.java.simpleName
	fun getRetrofitErrorMessage(response: Response<ResponseBody?>): String?
	{
		var resMsg: String? = null
		val converter = RetrofitAPI.retrofit.responseBodyConverter<RetrofitError>(
			RetrofitError::class.java, arrayOfNulls(0)
																				 )
		val errorBody = response.errorBody()
		try
		{
			var errorResponse: RetrofitError? = null
			if (errorBody != null)
			{
				val mediaType = MediaType.parse("application/json")
				var strErrorReason: String? = ""
				if (mediaType != null && mediaType == errorBody.contentType())
				{
					errorResponse = converter.convert(errorBody)
					strErrorReason += errorResponse.errorDetails
					resMsg = errorResponse.errorDetails
				} else
				{
					val strErrorBody = errorBody.string()
					val spanned = Html.fromHtml(strErrorBody)
					strErrorReason += spanned.toString()
					resMsg = spanned.toString()
				}
				if (!BuildConfig.DEBUG)
				{
					//Crashlytics.logException(RuntimeException(strErrorReason))
				}
			}
		} catch (e: Exception)
		{
			e.printStackTrace()
			//if (!BuildConfig.DEBUG) Crashlytics.logException(e)
			Log.d(BuildConfig.APPLICATION_ID, className + ": error" + e.message)
			resMsg = e.localizedMessage
		}
		return resMsg
	}

	fun logRetrofitErrorMessage(response: Response<ResponseBody?>, className: String)
	{
		val converter: Converter<ResponseBody, RetrofitError> =
			retrofit.responseBodyConverter(RetrofitError::class.java, arrayOfNulls<Annotation>(0))
		try
		{
			if (response.errorBody() != null)
			{
				val errorResponse: RetrofitError = converter.convert(response.errorBody())
				val strErrorReason = className + ": " + errorResponse.errorDetails
				Log.d(BuildConfig.APPLICATION_ID, strErrorReason)
				//FirebaseCrashlytics.getInstance().log(strErrorReason)
				response.errorBody()!!.close()
			}
		} catch (e: IOException)
		{
			//FirebaseCrashlytics.getInstance().recordException(e)
			e.printStackTrace()
		}
	}
}