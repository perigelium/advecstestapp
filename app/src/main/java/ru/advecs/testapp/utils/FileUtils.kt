package ru.advecs.testapp.utils

import android.content.Context
import android.net.Uri
import android.os.Environment
import android.provider.OpenableColumns
import android.text.TextUtils
import ru.advecs.testapp.App
import java.io.*

object FileUtils
{
    fun copyInputStreamToFile(`in`: InputStream?, file: File?)
    {
        var out: OutputStream? = null
        try
        {
            out = FileOutputStream(file)
            val buf = ByteArray(1024)
            var len: Int
            while (`in`!!.read(buf).also { len = it } > 0)
            {
                out.write(buf, 0, len)
            }
        } catch (e: Exception)
        {
        } finally
        {
            // Ensure that the InputStreams are closed even if there's an exception.
            try
            {
                out?.close()

                // If you want to close the "in" InputStream yourself then remove this
                // from here but ensure that you close it yourself eventually.
                `in`!!.close()
            } catch (e: IOException)
            {
            }
        }
    }

    fun createLocalFile(dirPath: String?, fname: String): File?
    {
        val dir = File(dirPath)
        if (TextUtils.isEmpty(fname)) return null
        if (!dir.exists())
        {
            dir.mkdirs()
            dir.setWritable(true, false)
        }
        return File("$dir/$fname")
    }

    @JvmStatic
    val imagesDir: File?
        get() = App.appInstance!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)

    @JvmStatic
    @Throws(IOException::class)
    fun copyFileFromUri(appCtx: Context, uri: Uri?, destDir: String?): File?
    {
        // The query, since it only applies to a single document, will only return
        // one row. There's no need to filter, sort, or select fields, since we want
        // all fields for one document.
        val cursor = appCtx.contentResolver.query(uri!!, null, null, null, null, null)
        try
        {
            // moveToFirst() returns false if the cursor has 0 rows.  Very handy for
            // "if there's anything to look at, look at it" conditionals.
            if (cursor != null && cursor.moveToFirst())
            {

                // Note it's called "Display Name".  This is
                // provider-specific, and might not necessarily be the file name.
                val displayName =
                    cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                val file = createLocalFile(destDir, displayName)
                val inputStream = appCtx.contentResolver.openInputStream(
                    uri
                                                                        )
                copyInputStreamToFile(inputStream, file)
                return file
            }
        } finally
        {
            cursor?.close()
        }
        return null
    }
}