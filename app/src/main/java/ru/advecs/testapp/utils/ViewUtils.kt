package ru.advecs.testapp.utils

import android.app.Activity
import android.graphics.Color
import android.view.Gravity
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import ru.advecs.testapp.R

object ViewUtils
{
	fun setProgressDialog(activity: Activity?): AlertDialog
	{
		val llPadding = 30
		val ll = LinearLayout(activity)
		ll.orientation = LinearLayout.HORIZONTAL
		ll.setPadding(llPadding, llPadding, llPadding, llPadding)
		ll.gravity = Gravity.CENTER
		var llParam = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
		                                        LinearLayout.LayoutParams.WRAP_CONTENT)
		llParam.gravity = Gravity.CENTER
		ll.layoutParams = llParam
		val progressBar = ProgressBar(activity)
		progressBar.isIndeterminate = true
		progressBar.setPadding(0, 0, llPadding, 0)
		progressBar.layoutParams = llParam
		llParam = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
		                                    ViewGroup.LayoutParams.WRAP_CONTENT)
		llParam.gravity = Gravity.CENTER
		val tvText = TextView(activity)
		tvText.setText(R.string.DownloadingDataPleaseWait)
		tvText.setTextColor(Color.parseColor("#000000"))
		tvText.textSize = 20f
		tvText.layoutParams = llParam
		ll.addView(progressBar)
		ll.addView(tvText)
		val builder = AlertDialog.Builder(activity!!)
		builder.setCancelable(true)
		builder.setView(ll)
		val dialog = builder.create()
		//dialog.show();
		val window = dialog.window
		if (window != null)
		{
			val layoutParams = WindowManager.LayoutParams()
			layoutParams.copyFrom(dialog.window!!.attributes)
			layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
			layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
			dialog.window!!.attributes = layoutParams
		}
		return dialog
	}
}